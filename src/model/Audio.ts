import * as React from 'react';

enum CourtPosition {
  TopLeft = 'topleft',
  TopRight = 'topright',
  BotLeft = 'botleft',
  BotRight = 'botright',
  Left = 'left',
  Right = 'right',
  Center = 'center',
  Front = 'front',
  Back = 'back',
}

enum AudioFilePath {
  Back = '/assets/back.mp3',
  Four = '/assets/four.mp3',
  Front = '/assets/front.mp3',
  Left = '/assets/left.mp3',
  Long = '/assets/long.mp3',
  One = '/assets/one.mp3',
  PossibleSmash = '/assets/possible_smash.mp3',
  Right = '/assets/right.mp3',
  Short = '/assets/short.mp3',
  Three = '/assets/three.mp3',
  Two = '/assets/two.mp3',
}

interface CourtPositionInfo {
  audioFilePath: AudioFilePath;
  position: CourtPosition;
}

// All the court possible positions and related information.
// exposed dict ensuring there is an element for every possible key values and associating the above types and enums between one another
// note: redundant CourtPosition between key type and value type for convenience
export const courtPositionInfos: Readonly<Record<CourtPosition, CourtPositionInfo>> = {
  topleft: { audioFilePath: AudioFilePath.One, position: CourtPosition.TopLeft },
  topright: { audioFilePath: AudioFilePath.Two, position: CourtPosition.TopRight },
  botleft: { audioFilePath: AudioFilePath.Three, position: CourtPosition.BotLeft },
  botright: { audioFilePath: AudioFilePath.Four, position: CourtPosition.BotRight },
  left: { audioFilePath: AudioFilePath.Left, position: CourtPosition.Left },
  right: { audioFilePath: AudioFilePath.Right, position: CourtPosition.Right },
  // TODO center: no audio file yet
  center: { audioFilePath: AudioFilePath.Front, position: CourtPosition.Center },
  front: { audioFilePath: AudioFilePath.Front, position: CourtPosition.Front },
  back: { audioFilePath: AudioFilePath.Back, position: CourtPosition.Back },
};

export function announceRandomCorner(
  audioRef: React.RefObject<HTMLAudioElement>
) {
  const audioTag = audioRef.current;
  if (audioTag) {
    // subset to select from, in case some of the possible values are not eligible, i.e., disabled by the user in the application parameters
    let courtPositionInfosSubset = [ courtPositionInfos.topleft, courtPositionInfos.topright, courtPositionInfos.botleft, courtPositionInfos.botright ];
    let randomIdx = Math.floor(Math.random() * courtPositionInfosSubset.length);
    audioTag.src = courtPositionInfosSubset[randomIdx].audioFilePath;
    audioTag.play();
  }
}
