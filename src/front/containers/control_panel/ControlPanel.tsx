import React from "react";
import * as AudioUtils from "../../../model/Audio";

interface ControlPanelProps {
  audioRef: React.RefObject<HTMLAudioElement>;
}

export default function ControlPanel({ audioRef }: ControlPanelProps) {
  // FIXME When the audio are being played, quickly clicking on Stop then on Play will make subsequent Play clicks fail.
  // Re-clicking on stop before trying to play again fails as well. Only refreshing the page or clicking + or - will
  // allow for a new Play click to be successful.

  // Null means no sound is being played at the moment
  let interval: NodeJS.Timer | null = null;
  const [intervalTimerInMs, setIntervalTimerInMs] =
    React.useState<number>(3000);

  function doStartPlaying() {
    if (interval != null) {
      return;
    }
    AudioUtils.announceRandomCorner(audioRef);
    interval = setInterval(
      () => AudioUtils.announceRandomCorner(audioRef),
      intervalTimerInMs
    );
  }

  function doStopPlaying() {
    interval != null && clearInterval(interval);
  }

  function doIncrementIntervalTimerInMs() {
    doStopPlaying();
    setIntervalTimerInMs(intervalTimerInMs + 100);
  }

  function doDecrementIntervalTimerInMs() {
    doStopPlaying();
    setIntervalTimerInMs(intervalTimerInMs - 100);
  }

  return (
    <div className="flex-col" style={{ marginTop: "8px" }}>
      <button className="btn w100" onClick={doStartPlaying} children="Start" />
      <button className="btn w100" onClick={doStopPlaying} children="Stop" />
      <div
        className="w100"
        style={{
          display: "flex",
          flexDirection: "row",
          gap: "8px",
        }}
      >
        <button
          className="btn w100"
          onClick={doDecrementIntervalTimerInMs}
          children="-"
        />
        <span
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            whiteSpace: "nowrap",
            // text
            fontWeight: "bolder",
          }}
        >
          {intervalTimerInMs / 1000}s between corners
        </span>
        <button
          className="btn w100"
          onClick={doIncrementIntervalTimerInMs}
          children="+"
        />
      </div>
    </div>
  );
}
