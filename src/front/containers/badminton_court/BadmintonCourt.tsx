import React from 'react';

export default function BadmintonCourt() {

  return (
    <div style={{ position: 'relative' }}>
      <div className="badminton-field" style={{ width: '99%' }}>
        <div style={{ minHeight: '100%', minWidth: '100%', backgroundColor: 'green', border: '2px solid #fff', boxSizing: 'border-box' }} />
        <div style={{ minHeight: '100%', minWidth: '100%', backgroundColor: 'green', border: '2px solid #fff', boxSizing: 'border-box', gridColumnStart: 2, gridColumnEnd: 4 }} />
        <div style={{ minHeight: '100%', minWidth: '100%', backgroundColor: 'green', border: '2px solid #fff', boxSizing: 'border-box' }} />
        <div style={{ minHeight: '100%', minWidth: '100%', backgroundColor: 'green', border: '2px solid #fff', boxSizing: 'border-box' }} />
        <div style={{ minHeight: '100%', minWidth: '100%', backgroundColor: 'green', border: '2px solid #fff', boxSizing: 'border-box' }} />
        <div style={{ minHeight: '100%', minWidth: '100%', backgroundColor: 'green', border: '2px solid #fff', boxSizing: 'border-box' }} />
        <div style={{ minHeight: '100%', minWidth: '100%', backgroundColor: 'green', border: '2px solid #fff', boxSizing: 'border-box' }} />
        <div style={{ minHeight: '100%', minWidth: '100%', backgroundColor: 'green', border: '2px solid #fff', boxSizing: 'border-box' }} />
        <div style={{ minHeight: '100%', minWidth: '100%', backgroundColor: 'green', border: '2px solid #fff', boxSizing: 'border-box' }} />
        <div style={{ minHeight: '100%', minWidth: '100%', backgroundColor: 'green', border: '2px solid #fff', boxSizing: 'border-box' }} />
        <div style={{ minHeight: '100%', minWidth: '100%', backgroundColor: 'green', border: '2px solid #fff', boxSizing: 'border-box' }} />
      </div>
      <div
        children="1"
        style={{
          // display
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          // position
          position: 'absolute',
          top: '5%',
          left: '10%',
          // other
          height: '50px',
          aspectRatio: '1 / 1',
          backgroundColor: 'blue',
          border: '2px solid #fff',
          borderRadius: '99px',
          // children text
          fontSize: '20px',
          fontWeight: 'bolder',
        }}
      />
      <div
        children="2"
        style={{
          // display
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          // position
          position: 'absolute',
          top: '5%',
          right: '10%',
          // other
          height: '50px',
          aspectRatio: '1 / 1',
          backgroundColor: 'blue',
          border: '2px solid #fff',
          borderRadius: '99px',
          // children text
          fontSize: '20px',
          fontWeight: 'bolder',
        }}
      />
      <div
        children="3"
        style={{
          // display
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          // position
          position: 'absolute',
          bottom: '5%',
          left: '10%',
          // other
          height: '50px',
          aspectRatio: '1 / 1',
          backgroundColor: 'blue',
          border: '2px solid #fff',
          borderRadius: '99px',
          // children text
          fontSize: '20px',
          fontWeight: 'bolder',
        }}
      />
      <div
        children="4"
        style={{
          // display
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          // position
          position: 'absolute',
          bottom: '5%',
          right: '10%',
          // other
          height: '50px',
          aspectRatio: '1 / 1',
          backgroundColor: 'blue',
          border: '2px solid #fff',
          borderRadius: '99px',
          // children text
          fontSize: '20px',
          fontWeight: 'bolder',
        }}
      />
    </div>
  );
}
