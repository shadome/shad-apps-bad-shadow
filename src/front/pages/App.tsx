import React from 'react';
import './App.css';
import BadmintonCourt from '../containers/badminton_court/BadmintonCourt';
import ControlPanel from '../containers/control_panel/ControlPanel';

enum State {
  SplitStep = 1,
  AnnouceCountdown,
  AnnounceSuccess,
  AnnounceCorner,
  AnnounceShort,
  AnnounceLong,
  WaitPlayerMovementQuick,
  WaitPlayerMovementSlow,
  PlayerStringSoundFaint,
  PlayerStringSoundLoud,
  WaitPlayerReplacementQuick,
  WaitPlayerReplacementSlow,
  AnnouncePossibleSmash,
  OpponentStringSoundFaint,
  OpponentStringSoundLoud,
}

function App() {

  // FIXME When the audio are being played, quickly clicking on Stop then on Play will make subsequent Play clicks fail.
  // Re-clicking on stop before trying to play again fails as well. Only refreshing the page or clicking + or - will
  // allow for a new Play click to be successful.

  const audioRef = React.useRef<HTMLAudioElement>(null);

  return (
    <div className="container">
      <audio ref={audioRef}>
        <source type="audio/mpeg" />
        Your browser does not support the audio element.
      </audio>
      <div className="flex-col" style={{ marginTop: '8px' }}>

        <BadmintonCourt />

        <ControlPanel audioRef={audioRef} />

      </div>
    </div>
  );
}

export default App;
